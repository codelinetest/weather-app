# Weather App
## Getting started

1. Go to project folder and install dependencies:
 ```
 npm install
 ```

2. Launch development server, and open `localhost:4200` in your browser:
 ```
 npm start
 ```
## Main tasks

Task automation is based on [NPM scripts](https://docs.npmjs.com/misc/scripts).

Task                            | Description
--------------------------------|--------------------------------------------------------------------------------------
`npm start`                     | Run development server on `http://localhost:4200/`
`npm run build [-- --configuration=production]` | Lint code and build web app for production (with [AOT](https://angular.io/guide/aot-compiler)) in `dist/` folder
`npm test`                      | Run unit tests via [Karma](https://karma-runner.github.io) in watch mode
`npm run test:ci`               | Lint code and run unit tests once for continuous integration
`npm run e2e`                   | Run e2e tests using [Protractor](http://www.protractortest.org)
`npm run lint`                  | Lint code
`npm run translations:extract`  | Extract strings from code and templates to `src/app/translations/template.json`
`npm run docs`                  | Display project documentation

## Development server

Run `npm start` for a dev server. Navigate to `http://localhost:4200/`. 

## Screenshots
### Home
![home](https://s3-ap-southeast-1.amazonaws.com/codeline-demo/weather-app-screenshot-home.png)
### Details Page
![details](https://s3-ap-southeast-1.amazonaws.com/codeline-demo/weather-app-screenshot-details.png)
### Search
![Search](https://s3-ap-southeast-1.amazonaws.com/codeline-demo/weather-app-screenshot-search.png)
