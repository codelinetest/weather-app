import { Component, OnInit } from '@angular/core';
import { ActivatedRoute } from '@angular/router';

@Component({
  selector: 'app-single',
  templateUrl: './single.component.html',
  styleUrls: ['./single.component.scss']
})
export class SingleComponent implements OnInit {
  woeid: string;

  constructor(private route: ActivatedRoute) { }

  ngOnInit() {
    this.woeid = this.route.snapshot.params['id'];
  }

}
