import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { TranslateModule } from '@ngx-translate/core';

import { CoreModule } from '@app/core';
import { SharedModule } from '@app/shared';
import { HomeRoutingModule } from './home-routing.module';
import { HomeComponent } from './home.component';
import { WeatherComponent } from '../weather/weather.component';
import { SingleComponent } from './single/single.component';
import { FindComponent } from './find/find.component';

@NgModule({
  imports: [
    CommonModule,
    TranslateModule,
    CoreModule,
    SharedModule,
    HomeRoutingModule
  ],
  declarations: [
    HomeComponent,
    WeatherComponent,
    SingleComponent,
    FindComponent
  ],
  providers: [
  ]
})
export class HomeModule { }
