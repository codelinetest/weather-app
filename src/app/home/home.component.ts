import { Component, OnInit } from '@angular/core';
import { finalize } from 'rxjs/operators';
@Component({
  selector: 'app-home',
  templateUrl: './home.component.html',
  styleUrls: ['./home.component.scss']
})
export class HomeComponent implements OnInit {

  isLoading: boolean;
  cities: Object[];

  constructor() {
    this.cities = [
      {
        name: "Istanbul",
        woeid: "2344116"
      },
      {
        name: "Berlin",
        woeid: "638242"
      },
      {
        name: "London",
        woeid: "44418"
      },
      {
        name: "Helsinki",
        woeid: "565346"
      },
      {
        name: "Dublin",
        woeid: "560743"
      },
      {
        name: "Vancouver",
        woeid: "9807"
      },
    ];
  }

  ngOnInit() {
    this.isLoading = true;
    
  }

}
