import { NgModule } from '@angular/core';
import { Routes, RouterModule } from '@angular/router';

import { extract } from '@app/core';
import { HomeComponent } from './home.component';
import { SingleComponent } from './single/single.component';
import { FindComponent } from './find/find.component';

import { Shell } from '@app/shell/shell.service';

const routes: Routes = [
  Shell.childRoutes([
    // { path: '', redirectTo: '/home', pathMatch: 'full' },
    { path: '', component: HomeComponent, data: { title: extract('Home') } },
    { path: 'search/:keyword', component: FindComponent },
    { path: 'woeid/:id', component: SingleComponent }


  ])
];

@NgModule({
  imports: [RouterModule.forChild(routes)],
  exports: [RouterModule],
  providers: []
})
export class HomeRoutingModule { }
