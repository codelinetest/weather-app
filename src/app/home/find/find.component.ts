import { Component, OnInit } from '@angular/core';
import { ActivatedRoute } from '@angular/router';
import { WeatherService } from '../../weather.service';
import { Observable } from 'rxjs';

import { debounceTime } from 'rxjs/operators'; 

@Component({
  selector: 'app-find',
  templateUrl: './find.component.html',
  styleUrls: ['./find.component.scss'],
  providers: [WeatherService]
})
export class FindComponent implements OnInit {

  keyword: string;
  public location: any[];
  public loading: boolean;
  

  constructor(private route: ActivatedRoute, private WeatherService: WeatherService) {
    this.route.params.subscribe(params => {
      this.loading = true;
      this.keyword = params['keyword'];
      this.getLocation();
    });
    
  }

  ngOnInit() {

  }

  async getLocation(){
    this.WeatherService.getLocation(this.keyword)
    .subscribe(
      (data) => {
        this.location = data;
        this.loading = false;
      });
  }


}
