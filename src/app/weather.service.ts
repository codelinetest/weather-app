import { Injectable } from '@angular/core';
import {HttpClient, HttpHeaders} from '@angular/common/http';

import {environment} from '../environments/environment';

import { Observable } from 'rxjs';
import { map } from 'rxjs/operators';

const API_URL = environment.serverUrl;

@Injectable({
  providedIn: 'root'
})
export class WeatherService {

  constructor(private http: HttpClient) { }

  getWeather(woeid: string): Observable<any>{
    let headers: HttpHeaders = new HttpHeaders();
    headers = headers.append('Content-Type', 'application/json');

    const httpOptions = {
      headers: headers
    };

    return this.http
      .get(API_URL + '?command=location&woeid=' + woeid, httpOptions)
      .pipe(
        map((data: any[]) => {
          console.log(data);

          return data;
      })
      )
  }

  getLocation(keyword: string): Observable<any>{
    let headers: HttpHeaders = new HttpHeaders();
    headers = headers.append('Content-Type', 'application/json');

    const httpOptions = {
      headers: headers
    };

    return this.http
      .get(API_URL + '?command=search&keyword=' + keyword, httpOptions)
      .pipe(
        map((data: any[]) => {
          return data;
      })
      )
  }
}
