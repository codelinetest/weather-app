import { Component, OnInit, Input } from '@angular/core';
import { WeatherService } from '../weather.service';
import { Observable } from 'rxjs';
@Component({
  selector: 'weather',
  templateUrl: './weather.component.html',
  styleUrls: ['./weather.component.scss'],
  providers: [WeatherService]
})
export class WeatherComponent implements OnInit {

  @Input()
    woeid: string;

  @Input()
    single: string;

  
  public weather: any[];

  constructor(private WeatherService: WeatherService) {
  }
  

  ngOnInit() {
    this.WeatherService.getWeather(this.woeid)
      .subscribe(
        (data) => {
          this.weather = data;
        });
      }
}
